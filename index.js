const express = require('express'); // Express
const Datastore = require('nedb-promise'); // Database
const fs = require('fs'); // File System
const socketio = require('socket.io'); // Socket.io
const http = require('http'); // HyperText Transfer Protocol
const config = require('./config.json'); // Config
const bodyParser = require('body-parser'); // POST Requests
const cookieParser = require('cookie-parser'); // Cookies
const hbs = require('handlebars'); // Templating Engine
const helpers = require('handlebars-helpers')({
	handlebars: hbs
}); // Handlebars Helpers

var app = express();
app.use(express.static('public')); // Public Files
app.use(bodyParser.json()); // POST Requests
app.use(bodyParser.urlencoded({ extended: true })); // POST Requests
app.use(cookieParser()); // Cookies

var db = {}; // Database Object
db.pages = new Datastore('db/page.db'); // Sets up Page DB
db.posts = new Datastore('db/blog.db'); // Sets up Blog Post DB
db.options = new Datastore('db/options.db'); // Sets up Options DB
db.pages.loadDatabase(); // Loads Page DB
db.posts.loadDatabase(); // Loads Blog Post DB
db.options.loadDatabase(); // Loads Options DB

app.all(/.+/,function(req, res){
	res.send('Hello, World');
});

var server = http.Server(app);
var io = socketio(server);
server.listen(config.port);
console.log(config.port);

console.log(config);
